<?php

namespace App\Http\Controllers;

use App\Models\ListItem;
use App\User;
use Illuminate\Http\Request;

class AngularController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function userByEmail(Request $request)
    {
        $user = User::whereEmail($request->get('email'))->first();

        return $user;
    }

    public function register(Request $request)
    {
        return $request->all();
    }

    public function bill()
    {
        return response([
            'currency' => 'RUB',
            'value' => 300
        ]);
    }

    public function getList()
    {
        return ListItem::pluck('text');
    }

    public function saveItem(Request $request)
    {
        $item = ListItem::create($request->all());

        return $item;
    }
}
